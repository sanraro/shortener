require 'test_helper'

class TagTest < ActiveSupport::TestCase
  test 'duplicated name is not allowed' do
    Tag.create(name: 'my tag test', created_by: users(:user_001))
    tag = Tag.new(name: 'my tag test', created_by: users(:user_001))
    assert_not tag.save
  end
end
