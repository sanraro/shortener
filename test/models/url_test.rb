require 'test_helper'

class UrlTest < ActiveSupport::TestCase
  test 'url is saved' do
    url = Url.new(name: 'my url', original_url: 'http://my.url', slug: 'my slug', created_by: users(:user_001))
    assert url.save
  end
  test 'duplicated slug is not allowed' do
    Url.create(name: 'my url', original_url: 'http://my.url', slug: 'admin', created_by: users(:user_001))
    url = Url.new(name: 'second url', original_url: 'http://my.url', slug: 'admin', created_by: users(:user_001))
    assert_not url.save
  end
  test 'admin slug is not allowed' do
    url = Url.new(name: 'my url', original_url: 'http://my.url', slug: 'admin',
                  created_by: users(:user_001))
    assert_not url.save
  end
  test 'enabled url is published' do
    url = Url.create(name: 'my url', original_url: 'http://my.url', slug: 'my slug',
                     created_by: users(:user_001))
    assert url.published?
    assert Url.published.find_by(id: url.id)
  end
  test 'not enabled url is not published' do
    url = Url.create(name: 'my url', original_url: 'http://my.url', slug: 'my slug',
                     is_enabled: false, created_by: users(:user_001))
    assert_not url.published?
    assert_not Url.published.find_by(id: url.id)
  end
  test 'on time url is published' do
    url = Url.create(name: 'my url', original_url: 'http://my.url', slug: 'my slug',
                     created_by: users(:user_001),
                     starts_at: (DateTime.now - 1.day), ends_at: (DateTime.now + 1.day))
    assert url.published?
    assert Url.published.find_by(id: url.id)
  end
  test 'expired url is not published' do
    url = Url.create(name: 'my url', original_url: 'http://my.url', slug: 'my slug',
                     created_by: users(:user_001),
                     starts_at: (DateTime.now - 2.day), ends_at: (DateTime.now - 1.day))
    assert_not url.published?
    assert_not Url.published.find_by(id: url.id)
  end
  test 'future url is not published' do
    url = Url.create(name: 'my url', original_url: 'http://my.url', slug: 'my slug',
                     created_by: users(:user_001),
                     starts_at: (DateTime.now + 1.day), ends_at: (DateTime.now + 2.day))
    assert_not url.published?
    assert_not Url.published.find_by(id: url.id)
  end
end
