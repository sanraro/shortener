require 'test_helper'

class UserTest < ActiveSupport::TestCase
  test 'user is saved' do
    url = User.new username: 'my name', email: 'email@domain.com',
                   password: 'mypass', password_confirmation: 'mypass'
    assert url.save
  end
  test 'duplicated email is not allowed' do
    User.create username: 'User 1', email: 'email@domain.com',
                password: 'mypass', password_confirmation: 'mypass'
    user = User.new username: 'User 2', email: 'email@domain.com',
                    password: 'mypass', password_confirmation: 'mypass'
    assert_not user.save
  end
end
