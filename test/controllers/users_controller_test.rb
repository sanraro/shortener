require "test_helper"

class UsersControllerTest < ActionDispatch::IntegrationTest
  test "anonymous users can't see users list" do
    get users_url
    assert_response 302
  end 
  test "registered users can't see users list" do
    get '/users/sign_in'
    sign_in users(:user_001)
    post user_session_url
    follow_redirect!
    assert_response :success
    get users_url
    assert_response 403 
  end 
  test "admin users can see users list" do
    get '/users/sign_in'
    sign_in users(:admin_001)
    post user_session_url
    follow_redirect!
    get users_url
    assert_response :success
    assert_select 'div#users-list'
    assert_select 'td', users(:user_001).username
  end 
  test "registered users can't see other user information" do
    get '/users/sign_in'
    sign_in users(:user_001)
    post user_session_url
    follow_redirect!
    get user_url( users(:user_002) )
    assert_response 403
  end 
  test "registered users can see own information" do
    get '/users/sign_in'
    sign_in users(:user_001)
    post user_session_url
    follow_redirect!
    get user_url( users(:user_001) )
    assert_response :success
  end 
  test "admin users can see other user information" do
    get '/users/sign_in'
    sign_in users(:admin_001)
    post user_session_url
    follow_redirect!
    get user_url( users(:user_001) )
    assert_response :success
    assert_select 'dd', users(:user_001).email
  end
  test "admins can create a new user" do
    get '/users/sign_in'
    sign_in users(:admin_001)
    post user_session_url
    follow_redirect!
    post users_url, params: {user: {username: 'New user', email: 'new@email.com', password: '12345678', password_confirmation: '12345678'}}
    assert_response :found
    user = assigns(:user)
    assert_not_nil user
    db_user = User.find_by(username: 'New user', email: 'new@email.com')
    assert_equal db_user, user
  end
  test "admins can edit a existing user" do
    get '/users/sign_in'
    sign_in users(:admin_001)
    post user_session_url
    follow_redirect!
    put user_url( users(:user_001) ), params: {user: {username: 'Changed username'}}
    assert_response :found
    db_user = User.find_by(id: users(:user_001).id)
    assert_equal db_user.username, 'Changed username'
  end
  test "registered users can't remove users" do
    get '/users/sign_in'
    sign_in users(:user_003)
    post user_session_url
    follow_redirect!
    delete user_url(:user_003)
    assert_response 403
  end
  test "admin users can delete users" do
    get '/users/sign_in'
    sign_in users(:admin_001)
    post user_session_url
    follow_redirect!
    delete user_url( users(:user_003) )
    assert_response :see_other
    assert_select 'td', false, users(:user_003).username
  end
end
