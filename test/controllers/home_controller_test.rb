require "test_helper"

class HomeControllerTest < ActionDispatch::IntegrationTest
  test "enabled URL can be redirected" do
    get "/#{urls(:url_001).slug}"
    assert_redirected_to urls(:url_001).original_url
  end
  test "disabled URL cant be redirected" do
    get "/#{urls(:url_002).slug}"
    assert_response 404
  end
end
