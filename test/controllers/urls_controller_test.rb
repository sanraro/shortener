require "test_helper"

class UrlsControllerTest < ActionDispatch::IntegrationTest
  test "registered users can see urls list" do
    get '/users/sign_in'
    sign_in users(:user_001)
    post user_session_url
    follow_redirect!
    get urls_url
    assert_response 200
    assert_select 'td', urls(:url_001).name
    assert_select 'td', urls(:url_002).name
  end 
  test "registered users can see url info" do
    get '/users/sign_in'
    sign_in users(:user_001)
    post user_session_url
    follow_redirect!
    get url_url( urls(:url_001) )
    assert_response :success
    assert_select 'dd', urls(:url_001).description
  end
  test "users can create a new url" do
    get '/users/sign_in'
    sign_in users(:user_001)
    post user_session_url
    follow_redirect!
    post urls_url, params: {url: {name: 'New URL test', original_url: 'http://new.com/'}}
    assert_response :found
    url = assigns(:url)
    assert_not_nil url
    db_url = Url.find_by(name: 'New URL test', original_url: 'http://new.com/')
    assert_equal db_url, url
  end
  test "users can edit a existing url" do
    get '/users/sign_in'
    sign_in users(:user_001)
    post user_session_url
    follow_redirect!
    put url_url( urls(:url_002) ), params: {url: {name: 'Changed URL name'}}
    assert_response :found
    db_url = Url.find_by(id: urls(:url_002).id)
    assert_equal db_url.name, 'Changed URL name'
  end
  test "users can delete urls" do
    get '/users/sign_in'
    sign_in users(:user_001)
    post user_session_url
    follow_redirect!
    delete url_url( urls(:url_003) )
    assert_response :see_other
    assert_select 'td', false, urls(:url_003).name
  end
end
