require 'test_helper'

class TagsControllerTest < ActionDispatch::IntegrationTest
  test 'users can see tags list' do
    get '/users/sign_in'
    sign_in users(:user_001)
    post user_session_url
    follow_redirect!
    get tags_url
    assert_response 200
    assert_select 'td', tags(:tag_001).name
    assert_select 'td', tags(:tag_002).name
  end 
  test 'users can see tag info' do
    get '/users/sign_in'
    sign_in users(:user_001)
    post user_session_url
    follow_redirect!
    get tag_url( tags(:tag_001) )
    assert_response :success
    assert_select 'dd', tags(:tag_001).description
  end
  test 'users can create a new tag' do
    get '/users/sign_in'
    sign_in users(:user_001)
    post user_session_url
    follow_redirect!
    post tags_url, params: {tag: {name: 'New tag', description: 'my description'}}
    assert_response :found
    tag = assigns(:tag)
    assert_not_nil tag 
    db_tag = Tag.find_by(name: 'New tag')
    assert_equal db_tag, tag 
  end
  test 'users can edit a existing tag' do
    get '/users/sign_in'
    sign_in users(:user_001)
    post user_session_url
    follow_redirect!
    put tag_url( tags(:tag_002) ), params: {tag: {name: 'Changed tag name'}}
    assert_response :found
    db_tag = Tag.find_by(id: tags(:tag_002).id)
    assert_equal db_tag.name, 'Changed tag name'
  end
  test 'users can delete tags' do
    get '/users/sign_in'
    sign_in users(:user_001)
    post user_session_url
    follow_redirect!
    delete tag_url( tags(:tag_003) )
    assert_response :see_other
    assert_select 'td', false, tags(:tag_003).name
  end
end
