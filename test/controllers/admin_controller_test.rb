require 'test_helper'

# Define url host to allow admin testing
ENV['URL_HOST'] = 'www.example.com'

class AdminControllerTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers

  test "anonymous request must be redirected to login" do
    get admin_url
    assert_response 302 
  end

  test "registered user should get index" do
    get '/users/sign_in'
    sign_in users(:user_001)
    post user_session_url
    follow_redirect!
    assert_response :success
    get admin_url
    assert_response :success
  end

  test "disabled user should not login" do
    get '/users/sign_in'
    sign_in users(:user_002)
    post user_session_url
    follow_redirect!
    assert_response :success
    get admin_url
    assert_response 302
  end
end
