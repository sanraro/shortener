source "https://rubygems.org"

ruby "3.2.3"

# Bundle edge Rails instead: gem "rails", github: "rails/rails", branch: "main"
gem "rails", "~> 7.1.3"

# The original asset pipeline for Rails [https://github.com/rails/sprockets-rails]
gem "sprockets-rails"

# Use mysql as the database for Active Record
gem "mysql2"

# Use the Puma web server [https://github.com/puma/puma]
gem "puma", ">= 5.0"

# Use JavaScript with ESM import maps [https://github.com/rails/importmap-rails]
gem "importmap-rails"

# Hotwire's SPA-like page accelerator [https://turbo.hotwired.dev]
gem "turbo-rails"

# Hotwire's modest JavaScript framework [https://stimulus.hotwired.dev]
gem "stimulus-rails"

# Build JSON APIs with ease [https://github.com/rails/jbuilder]
# gem "jbuilder"

# Authentication
gem 'devise'
gem 'devise-i18n'
# Oauth2
gem 'omniauth'
# Using own provider
gem 'omniauth-oauth2'
# Using google
gem 'omniauth-google-oauth2'
gem 'omniauth-rails_csrf_protection'

# Use Redis adapter to run Action Cable in production
#gem "redis", "~> 4.0.1"

# Use Kredis to get higher-level data types in Redis [https://github.com/rails/kredis]
# gem "kredis"

# Use Active Model has_secure_password [https://guides.rubyonrails.org/active_model_basics.html#securepassword]
# gem "bcrypt", "~> 3.1.7"

# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
#gem "tzinfo-data", platforms: %i[ windows jruby ]

# Reduces boot times through caching; required in config/boot.rb
gem "bootsnap", require: false

# Use Active Storage variants [https://guides.rubyonrails.org/active_storage_overview.html#transforming-images]
# gem "image_processing", "~> 1.2"

group :development, :test do
  # See https://guides.rubyonrails.org/debugging_rails_applications.html#debugging-with-the-debug-gem
  gem "debug", platforms: %i[ mri windows ]
  gem "faker"
end

group :development do
  # Use console on exceptions pages [https://github.com/rails/web-console]
  gem "web-console"

  # Add speed badges [https://github.com/MiniProfiler/rack-mini-profiler]
  # gem "rack-mini-profiler"

  # Speed up commands on slow machines / big apps [https://github.com/rails/spring]
  # gem "spring"

  # Preview email in the default browser instead of sending it
  gem 'letter_opener'

  # Lint
  gem 'rubocop', require: false 
end

group :test do
  gem "sqlite3", "~> 1.4"
  gem 'rails-controller-testing'
  gem "capybara"
  gem "selenium-webdriver"
end

# Twitter Bootstrap
gem 'bootstrap', '5.1.3'

# Font Awesome
gem 'font-awesome-rails'

# Pagination
gem 'kaminari'
gem 'kaminari-i18n'

# QR Code generator
gem 'rqrcode'

# UserAgent parser
gem 'user_agent_parser'

# Graphs library
gem 'chartkick'

# Group by days (timezone support must be installed on server)
# https://github.com/ankane/groupdate#for-mysql
# mysql_tzinfo_to_sql /usr/share/zoneinfo | mysql -u root mysql
gem 'groupdate'

# Geolocalization
gem 'maxmind-geoip2'
# Country codes
gem 'countries'
