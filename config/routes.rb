require 'admin_domain'

Rails.application.routes.draw do
  # Callback for omniauth oauth2
  constraints AdminDomain do
    devise_for :users, only: :omniauth_callbacks, controllers: {omniauth_callbacks: 'users/omniauth_callbacks'}
  end
  scope "(:locale)", locale: /en|es/, defaults: {locale: :es} do
    # Can be used by load balancers and uptime monitors to verify that the app is live.
    get "up" => "rails/health#show", as: :rails_health_check

    # Defines paths for admin mode
    constraints AdminDomain do
      devise_for :users, skip: :omniauth_callbacks
      #devise_for :users
      get 'admin', to: 'admin#index'
      resources :comments, only: [:create]
      resources :tags
      resources :urls do
        collection do
          get 'reload_slug'
        end
      end
      resources :users
    end

    # Defines the root path route ("/")
    root 'home#index'

    # Links routes
    get '/:slug', to: 'home#redirect', constraints: AppDomain
  end
end
