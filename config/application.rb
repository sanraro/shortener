require_relative "boot"

require "rails/all"

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module Shortener
  class Application < Rails::Application
    # Initialize configuration defaults for originally generated Rails version.
    config.load_defaults 7.1

    # Please, add to the `ignore` list any other `lib` subdirectories that do
    # not contain `.rb` files, or that should not be reloaded or eager loaded.
    # Common ones are `templates`, `generators`, or `middleware`, for example.
    config.autoload_lib(ignore: %w(assets tasks))

    # Configuration for the application, engines, and railties goes here.
    #
    # These settings can be overridden in specific environments using the files
    # in config/environments, which are processed later.
    #
    # config.time_zone = "Central Time (US & Canada)"
    # config.eager_load_paths << Rails.root.join("extras")

    # Permitted locales available for the application
    config.i18n.default_locale = :es
    config.i18n.available_locales = [:es, :en]

    # Hosts allowed (includes app and admin domains)
    require 'admin_domain'
    config.hosts << AppDomain.host
    config.hosts << AdminDomain.host

    config.action_mailer.smtp_settings = {
      address:              ENV['SMTP_HOSTNAME']||'localhost',
      port:                 ENV['SMTP_PORT']||25,
      domain:               ENV['SMTP_DOMAIN']||AdminDomain.host,
      user_name:            ENV['SMTP_USERNAME'],
      password:             ENV['SMTP_PASSWORD'],
      authentication:       ENV['SMTP_AUTHTYPE']||'plain',
      enable_starttls:      false
    }

  end
end
