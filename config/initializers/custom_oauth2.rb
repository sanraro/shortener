require 'omniauth-oauth2'

module OmniAuth
  module Strategies
    class CustomOauth2 < OmniAuth::Strategies::OAuth2
      option :name, :custom_oauth2
      option :provider_ignores_state, true
      option :client_options, {
             site: ENV['OAUTH2_PROVIDER_URL'],
             authorize_url: '/oauth/authorize',
             token_url: '/oauth/token'
      }
      option :authorize_params, {
        response_type: 'code'
      }

      uid do
        raw_info['id']
      end

      info do
        {
          name: raw_info['name'],
          email: raw_info['email'],
          #name: raw_info['nombre_completo'],
          #email: raw_info['correoe']
        }
      end

      def raw_info
        #Rails.logger.info access_token.get('/api/me.json').parsed.inspect
        @raw_info ||= access_token.get('/api/v1/me.json').parsed
      end

      def callback_url
        full_host + script_name + callback_path
      end
    end
  end
end
