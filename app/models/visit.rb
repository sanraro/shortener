#--
#
#################################################################################
# Shortener: Generate, use, manage and analyze short URLs.
# Copyright 2023 Santiago Ramos
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#################################################################################
#
#++

require 'net/http'
require 'maxmind/geoip2'

class Visit < ApplicationRecord
  belongs_to :url
  before_save :set_country_code

  def request
    request_geoip
  end

  def request_country_code
    code = GeoipService.country_code(ip_address) if ENV['GEOIP_ACCOUNT_ID'] && ENV['GEOIP_LICENSE_KEY']
    code ||= HostipService.country_code(ip_address)
    return code || 'XX'
  end

  def country
    ISO3166::Country.new(country_code)
  end

  private

  def set_country_code
    self.country_code = request_country_code.upcase
  end
end
