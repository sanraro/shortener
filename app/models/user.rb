#--
#
#################################################################################
# Shortener: Generate, use, manage and analyze short URLs.
# Copyright 2023 Santiago Ramos
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#################################################################################
#
#++

class User < ApplicationRecord
  devise :database_authenticatable, :lockable,
         :recoverable, :rememberable, :validatable,
         :trackable, :timeoutable,
         :omniauthable, omniauth_providers: [:google_oauth2, :custom_oauth2]

  has_many :urls, foreign_key: :created_by_id, dependent: :nullify
  has_many :updated_urls, class_name: 'Url', foreign_key: :updated_by_id, dependent: :nullify
  has_many :tags, foreign_key: :created_by_id, dependent: :nullify
  has_many :updated_tags, class_name: 'Tag', foreign_key: :updated_by_id, dependent: :nullify
  has_many :comments, as: :commentable, dependent: :destroy
  has_many :commentaries, class_name: 'Comment', foreign_key: :created_by_id, dependent: :nullify

  validates :username, presence: true, uniqueness: true

  scope :enabled,-> { where(is_enabled: true) }

  def self.from_omniauth(auth)
    Rails.logger.info "[User.from_omniauth] #{auth.inspect}"
    if user = find_by(email: auth.info.email, is_enabled: true)
      if (user.provider.nil? && user.uid.nil?) || (user.provider == auth.provider.to_s && user.uid == auth.uid.to_s)
        user.update provider: auth.provider, uid: auth.uid,
                    username: auth.info.name, avatar_url: auth.info.image
      else
        user = nil
      end
    end
    return user
  end

  def active_for_authentication?
    super and self.is_enabled?
  end

  def password_required?
    new_record? ? false : super
  end

  # Alias method
  def name
    username
  end
end
