#--
#
#################################################################################
# Shortener: Generate, use, manage and analyze short URLs.
# Copyright 2023 Santiago Ramos
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#################################################################################
#
#++

class Url < ApplicationRecord
  has_and_belongs_to_many :tags
  has_many :visits, dependent: :destroy
  has_many :comments, as: :commentable, dependent: :destroy
  belongs_to :created_by, class_name: 'User'
  belongs_to :updated_by, class_name: 'User', optional: true

  validates :name, presence: true
  validates :original_url, presence: true 
  validates :slug, presence: true, uniqueness: true, exclusion: { in: %w(admin users tags urls), message: "%{value} is reserved." }
  validate :validate_publication_dates

  after_initialize :check_slug

  scope :enabled,-> { where(is_enabled: true) }
  scope :published, -> { enabled. 
                         where("starts_at IS NULL OR starts_at < ?", DateTime.now).
                         where("ends_at IS NULL OR ends_at > ?", DateTime.now) }

  def published?
    is_enabled && ( starts_at.nil? || starts_at < DateTime.now ) && ( ends_at.nil? || ends_at > DateTime.now )
  end

  def advanced_options?
    description.present? || starts_at || ends_at
  end

  def short_url
    AppDomain.base_url + '/' + slug
  end

  def register_visit request
    user_agent = UserAgentParser.parse(request.user_agent)
    visits.create ip_address: request.remote_ip,
                  referer: request.referer,
                  user_agent: request.user_agent,
                  browser_name: user_agent.family,
                  browser_version: user_agent.version.to_s,
                  os: user_agent.os.to_s,
                  device_brand: user_agent.device&.brand,
                  device_model: user_agent.device&.model
  end

  def qrcode
    RQRCode::QRCode.new(short_url).as_png(size: 300)
  end

  def qrcode_url
    qrcode.to_data_url
  end

  private

  def check_slug
    while self.slug.blank?
      slug_attempt = SecureRandom.alphanumeric(8).downcase
      self.slug = slug_attempt unless Url.where(slug: slug_attempt).any?
    end
  end

  def validate_publication_dates
    if starts_at.present? && ends_at.present? && starts_at > ends_at
      errors.add :base, "starting date can't be after endind date"
    end
  end
end
