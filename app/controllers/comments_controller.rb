class CommentsController < ApplicationController
  before_action :authenticate_user!

  def create
    comment = Comment.new(comment_params)
    comment.created_by = current_user
    comment.save
    respond_to do |format|
      format.html { @comment.commentable }
      format.turbo_stream do
        render turbo_stream: turbo_stream.replace('comments-container', partial: 'comments/comments', locals: {object: comment.commentable})
      end
    end
  end

  private

  def comment_params
    params.require(:comment).permit(:annotation, :commentable_type, :commentable_id)
  end
end
