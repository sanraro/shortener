class Users::OmniauthCallbacksController < Devise::OmniauthCallbacksController
  def custom_oauth2
    Rails.logger.info "[Users::OmniauthCallbackController.custom_oauth2]"
    if ENV['OAUTH2_PROVIDER_URL'].present? && ENV['OAUTH2_CLIENT_ID'].present? && ENV['OAUTH2_CLIENT_SECRET'].present?
      common_callback 'devise.custom_provider_data'
    else
      message = 'Client and secred tokens for Custom OAuth2 missing'
      Rails.logger.error "[Users::OmniauthCallbacksController.custom_oauth2] ERROR: " + message
      redirect_to new_user_session_url, alert: message 
    end
  end

  def google_oauth2
    Rails.logger.info "[Users::OmniauthCallbackController.google_oauth2]"
    if ENV['GOOGLE_CLIENT_ID'].present? && ENV['GOOGLE_CLIENT_SECRET'].present?
      common_callback 'devise.google_data'
    else
      message = 'Client and secred tokens for Google OAuth2 missing'
      Rails.logger.error "[Users::OmniauthCallbacksController.google_oauth2] ERROR: " + message
      redirect_to new_user_session_url, alert: message 
    end
  end

  def failure
    Rails.logger.info "[Users::OmniauthCallbackController.failure"
    Rails.logger.error "[Users::OmniauthCallbacksController.failure] ERROR!!!"
    redirect_to root_path
  end

  private

  def common_callback session_store
    # You need to implement the method below in your model (e.g. app/models/user.rb)
    @user = User.from_omniauth(request.env['omniauth.auth'])

    if @user && @user.persisted?
      flash[:notice] = I18n.t 'devise.omniauth_callbacks.success'
      sign_in_and_redirect @user, event: :authentication
    else
      # Set session info
      request_auth = request.env['omniauth.auth'].except('extra') # removing extra as it can overflow some session stores
      session[session_store] = request_auth
      message = @user ? t('devise.sessions.oauth2.error.disabled') : t('devise.sessions.oauth2.error.not-found')
      Rails.logger.error "[Users::OmniauthCallbacksController.common_callback] ERROR: " + message
      redirect_to new_user_session_url, alert: message
    end
  end

end
