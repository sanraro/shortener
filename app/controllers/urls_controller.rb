class UrlsController < ApplicationController
  before_action :authenticate_user!
  before_action :get_url, only: [:show, :edit, :update, :destroy]

  def index
    @urls = Url.reorder(:name).page params[:page]
  end

  def show
  end

  def new
    @url = Url.new
    @tags = Tag.order(:name)
  end

  def edit
    @tags = Tag.order(:name)
  end

  def create
    @url = Url.new(url_params)
    @url.created_by = current_user
    if @url.save
      flash[:notice] = t('urls.messages.removed')
      redirect_to @url
    else
      render :new, status: :unprocessable_entity
    end
  end

  def update
    @url.updated_by = current_user
    if @url.update(url_params)
      flash[:notice] = t('urls.messages.updated')
      redirect_to @url
    else
      render :edit, status: :unprocessable_entity
    end
  end

  def destroy
    @url.destroy
    if @url.errors.empty?
      flash[:notice] = t('urls.messages.removed')
    else
      flash[:alert] = @url.errors.full_messages
    end
    redirect_to urls_url, status: :see_other
  end

  def reload_slug
    url = Url.new
    render json: url.slug.to_json
  end
  
  private

  def get_url
    @url = Url.find(params[:id])
  end

  def url_params
    params.require(:url).permit( :name, :description, :slug, :original_url,
                                 :is_enabled, :starts_at, :ends_at,
                                 {tag_ids: []} )
  end

end
