class TagsController < ApplicationController
  before_action :authenticate_user!
  before_action :get_tag, only: [:show, :edit, :update, :destroy]

  def index
    @tags = Tag.reorder(:name).page params[:page]
  end

  def show
  end

  def new
    @tag = Tag.new
  end

  def edit
  end

  def create
    @tag = Tag.new(tag_params)
    @tag.created_by = current_user
    if @tag.save
      flash[:notice] = t('tags.messages.created')
      redirect_to @tag
    else
      render :new, status: :unprocessable_entity
    end
  end

  def update
    @tag.updated_by = current_user
    if @tag.update(tag_params)
      flash[:notice] = t('tags.messages.updated')
      redirect_to @tag
    else
      render :edit, status: :unprocessable_entity
    end
  end

  def destroy
    @tag.destroy
    if @tag.errors.empty?
      flash[:notice] = t('tags.messages.removed')
    else
      flash[:alert] = @tag.errors.full_messages
    end
    redirect_to tags_url, status: :see_other
  end

  private

  def get_tag
    @tag = Tag.find(params[:id])
  end

  def tag_params
    params.require(:tag).permit(:name, :description)
  end
end
