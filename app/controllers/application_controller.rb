#--
#
#################################################################################
# Shortener: Generate, use, manage and analyze short URLs.
# Copyright 2023 Santiago Ramos
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#################################################################################
#
#++

class ApplicationController < ActionController::Base
  around_action :switch_locale

  def default_url_options
    { locale: I18n.locale }
  end

  private
  
  def switch_locale(&action)
    #puts "*********** Params: " + params[:locale].inspect
    #puts "*********** &action: " + action.inspect
    locale = params[:locale] || I18n.default_locale
    I18n.with_locale(locale, &action)
  end

  def only_for_admin!
    head :forbidden unless current_user.is_admin?
  end
end
