class UsersController < ApplicationController
  before_action :authenticate_user!
  before_action :only_for_admin!, only: [:index, :new, :create, :destroy]
  before_action :get_user, only: [:show, :edit, :update, :destroy]

  def index
    @users = User.reorder(:username).page params[:page]
  end

  def show
    only_for_admin! if current_user != @user
  end

  def new
    @user = User.new
  end

  def edit
    only_for_admin! if current_user != @user
  end

  def create
    @user = User.new(user_params)
    if @user.save
      flash[:notice] = t('users.messages.created')
      redirect_to @user
    else
      render :new, status: :unprocessable_entity
    end
  end

  def update
    only_for_admin! if current_user != @user
    if ( user_params[:password].blank? && user_params[:password_confirmation].blank? ?
         @user.update_without_password(user_params) :
         @user.update(user_params)
       )
      flash[:notice] = t('users.messages.updated')
      redirect_to @user
    else
      render :edit, status: :unprocessable_entity
    end
  end

  def destroy
    if current_user != @user
      @user.destroy
    else
      flash[:alert] = t('users.errors.destroy.own-user')
    end
    if @user.errors.empty?
      flash[:notice] = t('users.messages.removed')
    else
      flash[:alert] = @user.errors.full_messages
    end
    redirect_to users_url, status: :see_other
  end

  private

  def get_user
    @user = User.find(params[:id])
  end

  def user_params
    allowed_params  = [:username, :email, :password, :password_confirmation]
    allowed_params += [:is_enabled, :is_admin] if current_user.is_admin?
    params.require(:user).permit allowed_params
  end
end
