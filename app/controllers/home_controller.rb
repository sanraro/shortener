class HomeController < ApplicationController
  def index
    @full_width = true
  end
  def redirect
    url = Url.published.find_by(slug: params[:slug])
    if url
      redirect_to url.original_url, status: :moved_permanently, allow_other_host: true
      url.register_visit request
    else
      render :redirect, status: 404
    end
  end
end
