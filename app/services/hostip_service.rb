class HostipService < IpService
  def country_code
    Rails.logger.info "[HostipService.country_code] Asking HostIP API for address #{@ip_address}"
    begin
      uri = URI.parse("http://api.hostip.info/country.php?ip=#{@ip_address}")
      response = Net::HTTP.get_response(uri) 
      return response.body if response && response.class.name == 'Net::HTTPOK'
    rescue => e
      Rails.logger.error "[HostipService.country_code] Error: " + e.inspect
    end
  end
end