class GeoipService < IpService
  def country_code
    Rails.logger.info "[GeoipService.country_code] Asking Maxmind GeoIP API for address #{@ip_address}"
    client = MaxMind::GeoIP2::Client.new(
      account_id: ENV['GEOIP_ACCOUNT_ID'],
      license_key: ENV['GEOIP_LICENSE_KEY'],
      host: 'geolite.info'
    )
    record = client.country(@ip_address)
    return record.country.iso_code if record.country
  end
end