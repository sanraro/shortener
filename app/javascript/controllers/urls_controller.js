import { Controller } from "@hotwired/stimulus"

export default class extends Controller {

  static targets = ["slug"]
  
  reload_slug() {
    fetch('/urls/reload_slug')
      .then(response => response.json())
      .then(data => this.slugTarget.value = data)
  }
}
