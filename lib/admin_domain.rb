class AdminDomain
  def self.matches?(request)
    (ENV['RAILS_ENV']=='development' && request.domain == 'admin.localhost') || request.host == self.host
  end
  def self.host
    ENV['URL_HOST_ADMIN'] || ENV['URL_HOST'] || 'localhost'
  end
  def self.base_url
    protocol = ENV['URL_PROTOCOL'] || 'http'
    port = ENV['URL_PORT'] || (ENV['RAILS_ENV'] == 'development' ? '3000' : '80')
    port_url = (port=='80' && protocol == 'http') || (port=='443' && protocol == 'https') ? '' : ':' + port
    "#{protocol}://#{self.host}" + port_url
  end
  def self.admin_url
    self.base_url + '/admin'
  end
end

class AppDomain
  def self.matches?(request)
    request.host == self.host
  end
  def self.host
    ENV['URL_HOST'] || 'localhost'
  end
  def self.port
    ENV['URL_PORT'] || (ENV['RAILS_ENV'] == 'development' ? '3000' : '80')
  end
  def self.base_url
    protocol = ENV['URL_PROTOCOL'] || 'http'
    port = ENV['URL_PORT'] || (ENV['RAILS_ENV'] == 'development' ? '3000' : '80')
    port_url = (port=='80' && protocol == 'http') || (port=='443' && protocol == 'https') ? '' : ':' + port
    "#{protocol}://#{self.host}" + port_url
  end
end

