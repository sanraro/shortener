# encoding: UTF-8


namespace :test_data do
  desc 'Create fake data for testing purpouses'
  task create: :environment do
    puts '\n\n'
    puts '***************************************************'
    puts '********** CREATE FAKE DATA FOR TESTING ***********'
    puts '***************************************************'
    puts ''

    puts '... creating 5 users'
    5.times.each do
      User.create username: Faker::Name.name_with_middle,
                  email: Faker::Internet.email(domain: 'domain.com'),
                  password: Faker::Internet.password
    end
    puts '... creating 10 comments for users'
    10.times.each do
      Comment.create commentable: User.order('RAND()').last,
                     annotation: Faker::Lorem.paragraph,
                     created_by_id: User.order('RAND()').first.id
    end
    puts '... creating 5 tags'
    5.times.each do
      Tag.create name: Faker::Lorem.words(number: rand(1..2)).join(' '),
                 description: Faker::Lorem.words(number: rand(2..10)).join(' ').capitalize,
                 created_by_id: User.order('RAND()').first.id
    end
    puts '... creating 10 comments for tags'
    10.times.each do
      Comment.create commentable: Tag.order('RAND()').last,
                     annotation: Faker::Lorem.paragraph,
                     created_by_id: User.order('RAND()').first.id
    end
    puts '... creating 10 URLs'
    10.times.each do
      url = Url.create name: Faker::Lorem.words(number: rand(2..4)).join(' ').capitalize,
                       original_url: Faker::Internet.url,
                       slug: Faker::Alphanumeric.alpha(number: 10),
                       created_by_id: User.order('RAND()').first.id
      rand(0..3).times do
        url.tags << Tag.order('RAND()').first
      end
    end
    puts '... creating 30 comments for URLs'
    30.times.each do
      Comment.create commentable: Url.order('RAND()').last,
                     annotation: Faker::Lorem.paragraph,
                     created_by_id: User.order('RAND()').first.id
    end
    puts '... creating 10.000 visits'
    1000.times.each do
      datetime = Faker::Time.between(from: DateTime.now - 1.month, to: DateTime.now)
      user_agent = Faker::Internet.user_agent
      ua = UserAgentParser.parse(user_agent)    
      Visit.create url_id: Url.order('RAND()').first.id,
                   ip_address: Faker::Internet.ip_v4_address,
                   user_agent: user_agent,
                   browser_name: ua.family,
                   browser_version: ua.version.to_s,
                   os: ua.os.to_s,
                   device_brand: ua.device&.brand.to_s,
                   device_model: ua.device&.model.to_s,
                   referer: Faker::Internet.url,
                   created_at: datetime
    end
  end
end

