# Default user
if User.all.empty?
  User.create username: 'Default admin', is_admin: true,
              email: 'admin@admin.com',
              password: 'Change.Me!', password_confirmation: 'Change.Me!'
end
# First Tag
if Tag.all.empty?
  Tag.create name: 'default', created_by_id: User.first.id
end
# First URL
if Url.all.empty?
  Url.create name: 'UOC', original_url: 'https://uoc.edu', slug: 'uoc', created_by_id: User.first.id
end
