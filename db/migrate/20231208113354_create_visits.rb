class CreateVisits < ActiveRecord::Migration[7.1]
  def change
    create_table :visits do |t|
      t.references :url, null: false, foreign_key: true
      t.string :ip_address
      t.text :user_agent
      t.string :browser_name
      t.string :browser_version
      t.string :os
      t.string :device_brand
      t.string :device_model
      t.text :referer

      t.timestamps
    end
  end
end
