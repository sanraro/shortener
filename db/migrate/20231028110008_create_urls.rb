class CreateUrls < ActiveRecord::Migration[7.1]
  def change
    create_table :urls do |t|
      t.string :name, null: false
      t.text   :description
      t.string :original_url, null: false
      t.string :slug, null: false
      t.boolean :is_enabled, null: false, default: true
      t.references :created_by, foreign_key: { to_table: :users }
      t.references :updated_by, foreign_key: { to_table: :users }
      t.datetime :starts_at
      t.datetime :ends_at      

      t.timestamps
    end
  end
end
