class CreateComments < ActiveRecord::Migration[7.1]
  def change
    create_table :comments do |t|
      t.references :commentable, polymorphic: true, null: false
      t.text :annotation, null:false
      t.references :created_by, foreign_key: { to_table: :users }

      t.timestamps
    end
  end
end
