class AddCountryToVisit < ActiveRecord::Migration[7.1]
  def change
    add_column :visits, :country_code, :string
  end
end
