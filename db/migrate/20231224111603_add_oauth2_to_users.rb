class AddOauth2ToUsers < ActiveRecord::Migration[7.1]
  def change
    add_column :users, :uid, :string
    add_column :users, :provider, :string
    add_column :users, :avatar_url, :string
  end
end
