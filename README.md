
[[_TOC_]]

# Shortener: a short URL manager

Shortener is a web service that allows the generation, from URLs provided by the user, of shorter URLs. This will enable the publication and use of these more compact routes in other web services and media like printed publications.  Thereby, this web service will receive this requests and will be responsible for redirecting the user to the original URL.

The resulting web service has a public site that will handle the redirections and collect information sent by visitor’s web browser. The administration and management of the URLs generated, as well as the analysis of the requests made and the characterization and classification of the visits will be done into a private component.

## Install Shortener locally (developer mode)

### Pre-requisites

There are some applications and libraries used by application that must be installed on the system, from the libraries needed to build the database connector to nodejs, yarn and libvips used to manage css, javascript and images.

Depending of OS used there are different methods to install this dependencies and it is not the purpose of this document to describe the steps necessary to install it on all possible systems.

Thus, current document will describe install process over a Debian based OS, like Ubuntu.

For a debian based OS (ie. Ubuntu) the required packages to be installed are:

    $ sudo apt install tzdata build-essential libmariadb-dev libvips pkg-config libpq-dev nodejs yarn

### Shortener application

Shortener application is build on top Ruby On Rails framework. To install it is recommended to use RVM (Ruby enVironment Manager) application: https://rvm.io/ . Required Ruby version is 3.2.2 .

With Ruby successfully installed source code can be downloaded as a .zip file:

    $ wget https://gitlab.com/sanraro/shortener/-/archive/main/shortener-main.zip

 or using git from GitLab service:

    $ git clone https://gitlab.com/sanraro/shortener.git

Then, from the app directory, install rails and all required gems:

    $ gem install bundler && bundle install

### Database (MariaDB)

A MariaDB database (former MySQL) is needed to store persistent information about users, urls and requests.

Create a new empty database and a user to access to it. To do that, from MariaDB (MySQL) console and using root user, execute following commands:

    CREATE DATABASE shortener;
    GRANT ALL ON shortener.* TO 'shortener'@'localhost' identified by 'shortener';

If you want to change any of this credentials values, modify it on previous SQL sentences and define a new environment variable with proper values:

    DB_HOST=localhost
    DB_NAME=shortener
    DB_USER=shortener
    DB_PASS=shortener

Then, create database structure and populate it with initial data:

    $ rake db:migrate
    $ rake db:seed

This will create database tables and an the initial admin user:

* email: admin@admin.com
* password: Change.Me!

### Precompile assets

To run the application in *production* mode (see RAILS_ENV in *Extra configurations* section) the application assets (css, javascript and static images), ruby code and used libraries must have been previously *pre-compiled*.

To do that run from command line the following sentences:

    $ bundle exec bootsnap precompile --gemfile
    $ bundle exec bootsnap precompile app/ lib/
    $ SECRET_KEY_BASE_DUMMY=1 ./bin/rails assets:precompile

### Start application

From command line, execute rails server:

    $ rails s

Then, the application can be opened on the browser using the URL http://localhost:3000/

## Install Shortener using docker (Service mode)

The easiest way to install the application is through docker packages.

In the application code there are a *Dockerfile* building script file and a docker compose orchestrator (*docker-compose.yml).

To build docker image and run service, execute following command:

    $ docker compose up

and it will be available on the following URL: http://localhost:3000/ .

(you can also add a *-d* flag to demonize it)

The *docker-compose.yml* file contains all configurations and dependencies needed to run the shortener components: application, database and smtp services.

Behaviour of the service can be changed modifying variables in the *environment* section of the three services as is explained in next section.

## Extra configurations

Shortener application is built following Rails principle of convencion over configuration. However many features can be changed through the following environment variables:

### Application mode: **RAILS_ENV variable**

Allows to define the behaviour of the application (classes cache, assets compilation, etc..). Allowed values are *development*, *production* and *test*.

Default value:

    RAILS_ENV=development

### Database configuration: **DB_ variables**

The database credentials can be defined using DB_ variables. Default values are:

    DB_HOST=localhost
    DB_NAME=shortener
    DB_USER=shortener
    DB_PASS=shortener

### Application domains: **URL_ variables**

This is used to define the domains of running application and generate short URLs.

The *URL_HOST* variable defines the domain used for the public zone of the application and is used to build short URLs.

The *URL_HOST_ADMIN* variable defines the domain used for the private zone, where the application can be managed.

Also, in production environments, it is important to define *URL_PORT* and *URL_PROCOL*. Recommended values for online services are *443* and *https* respectively.

Default values are:

    URL_HOST=localhost
    URL_HOST_ADMIN=localhost
    URL_PORT=3000
    URL_PROTOCOL=http

### Mail configuration: **SMTP_ variables**

In some situations application sends a email to recover user password or to send unlock instructions for a blocked user. To allow sending email messages some environment variables must be defined:

    SMTP_HOSTNAME=localhost
    SMTP_PORT=25
    SMTP_DOMAIN=localhost
    SMTP_USERNAME=no-reply
    SMTP_PASSWORD=change-me


### Oauth2 identification: Configure Google provider

The default user identification method is using email and password credentials, but is also allowed user identification using Oauth2 protocol against Google provider.

To enable Oauth2 identification using Google accounts you must register application into developers section:

* Go to 'https://console.developers.google.com'
* Select own project
* Select "Credential" option on "APIs & Services" section
* Create a new "OAuth Client ID" credential with:
 * "Web application" as Application type
 * https://youradmindomain as "Authorized JavaScript origins"
 * https://youradmindomain/users/auth/google_oauth2/callback as "Authorized redirect URI" 

Remember that "youradmindomain" must be the same that were defined by *URL_HOST_ADMIN* variable in *Application domains* section.

After that, use following environment variables to provide application id and secret key:

    GOOGLE_CLIENT_ID=google-client-application-id
    GOOGLE_CLIENT_SECRET=google-client-secret-key

### Oauth2 identification: Configure generic OAuth2 provider

Another allowed identification is using Oauth2 protocol against generic OAuth2 provider.

To enable it you must register application into provider OAuth2 application section and get client and secret keys.

The way you do it depends on each application, but in any way you will have to provide shortener redirection URI as "https://youradmindomain/users/auth/custom_oauth2/callback".

Remember that "youradmindomain" must be the same that were defined by *URL_HOST_ADMIN* variable in *Application domains* section.

After that, use following environment variables to provide application id and secret key:

    OAUTH2_PROVIDER_NAME=oauth2-provider-name
    OAUTH2_PROVIDER_URL=oauth2-provider-url
    OAUTH2_CLIENT_ID=application-id-from-oauth2-provider
    OAUTH2_CLIENT_SECRET=secret-key-from-oauth2-provider
    OAUTH2_CLIENT_SCOPE=configured-scope

### GeoIP requests configuration

There are two available ways to resolve geo location of the IP addreses obtained from the short URLs requests.

First one is using Maxmind GeoLite2 API service (https://dev.maxmind.com/geoip/geolite2-free-geolocation-data). This is a free geoip service with a limit of 1000 daily requests and it can be upgraded to paid GeoIP2 service if needed.

To use it you have to create a user account and generate a License key with it. After that, define following environment variables with proper values to enable geoip requests:

    GEOIP_ACCOUNT_ID=maxmind-geoip-account-id
    GEOIP_LICENSE_KEY=maxmind-geoip-license-key

Second method is using HostIP API info (http://hostip.info/). Because there is no need to register the application to make queries, this is the alternative to the first method if there is no API key for it.

## Generate fake data for testing

If you want to test application with fake information to see admin interfaces with some data, invoke seeds command on development environment:

    $ RAILS_ENV='development' rake test_data:create

